This project consists of scraping a tennis prediction website and compute
if it is worth it to bet on some tennis games.

Moreover the implementation allows the user to simulate the bets over a period
and to determine if he can earn money. In the simulation it is possible
to filter the games with parameters representing maximum and minimum prediction
coefficient. We can also limit the odds
