#!/usr/bin/env python3

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup

url_base = "http://www.tennisprediction.com/?year=2019&month=5&day="

max_money = -99999999
best_odd_threshold = 0
best_percentage_threshold = 0

for percentage_threshold in range(600, 650, 25):
    percentage_threshold /= 10
    for odd_threshold in range(175, 225, 25):
        odd_threshold /= 100
        money = 0
        nb_games = 0
        for day in range(1, 27):
            url = url_base + str(day)
            page = urlopen(url).read()
            soup = BeautifulSoup(page, "html.parser")
            tables = soup.find_all("table", {"id": "main_tur"})


            for table in tables:
                arr = []
                arr.append(table.find_all("tr", {"class": "match"}))
                arr.append(table.find_all("tr", {"class": "match1"}))
                for index in range(len(arr)):
                    rows = arr[index]
                    for i in range(len(rows)):
                        played = rows[i].find("td", {"class": "main_res_f"}).text
                        perc = rows[i].find("td", {"class": "main_perc"}).text
                        odd_ = rows[i].find("td", {"class": "main_odds_m"}).text
                        if played == "" or perc == "" or odd_ == "":
                            continue
                        percentage = float(perc.split("%")[0])
                        odd = float(odd_)
                        if odd < odd_threshold or percentage <= percentage_threshold:
                            continue 
                        print(percentage, odd)
                        if i % 2 == 0:
                            money += odd
                        nb_games += 1
 
        money -= nb_games
        print("There are {} games".format(nb_games))
        print("The money earned would be {}".format(money))
        if money > max_money:
            best_odd_threshold = odd_threshold
            best_percentage_threshold = percentage_threshold
            max_money = money


print("The maximum money earned would be {}, with odd threshold: {} and percentage_threshold: {}".format(max_money, best_odd_threshold, best_percentage_threshold))
